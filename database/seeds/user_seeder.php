<?php

use Illuminate\Database\Seeder;
use App\User;

class user_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
          'username' => 'devidso',
          'password' => app('hash')->make('asdasd'),
          'name' => 'Deividas'
        ]);
    }
}
