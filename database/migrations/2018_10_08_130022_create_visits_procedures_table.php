<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsProceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits_procedures', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('visit_id')->nullable();
            $table->foreign('visit_id')->references('id')->on('visits')->onDelete('cascade');
            $table->unsignedInteger('procedure_id')->nullable();
            $table->foreign('procedure_id')->references('id')->on('procedures')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits_procedures');
    }
}
