<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits_products', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('visit_id')->nullable();
          $table->foreign('visit_id')->references('id')->on('visits')->onDelete('cascade');
          $table->unsignedInteger('product_id')->nullable();
          $table->foreign('product_id')->references('id')->on('products')->onDelete('set null');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits_products');
    }
}
