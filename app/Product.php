<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'warehouse_category_id', 'qty', 'unit', 'unit_price'
    ];

    public function category(){
      return $this->belongsTo('App\WarehouseCategory', 'warehouse_category_id', 'id');
    }
}
