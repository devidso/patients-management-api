<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitProcedure extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'visit_id', 'procedure_id'
    ];

    protected $table = 'visits_procedures';

    public function procedure(){
      return $this->belongsTo('App\Procedure');
    }
}
