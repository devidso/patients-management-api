<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procedure extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'price', 'procedure_category_id'
    ];

    public function category(){
      return $this->belongsTo('App\ProcedureCategory', 'procedure_category_id', 'id');
    }
}
