<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcedureCategory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

    public function procedures(){
      return $this->hasMany('App\Procedure');
    }

}
