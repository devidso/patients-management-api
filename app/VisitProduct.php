<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitProduct extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'visit_id', 'product_id'
    ];

    protected $table = 'visits_products';

    public function product(){
      return $this->belongsTo('App\Product');
    }
}
