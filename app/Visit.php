<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start', 'end', 'filled', 'patient_id', 'doctor_id'
    ];

    public function doctor(){
      return $this->belongsTo('App\Doctor');
    }

    public function patient(){
      return $this->belongsTo('App\Patient');
    }

    public function procedure(){
      return $this->hasMany('App\VisitProcedure');
    }

    public function product(){
      return $this->hasMany('App\VisitProduct');
    }
}
