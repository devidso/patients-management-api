<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visit;
use App\Doctor;

class VisitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $r){
      if($r->input('doctor')){
        return Visit::where('doctor_id', $r->input('doctor'))->with(['doctor', 'patient', 'procedure', 'product'])->get();
      }
      if($r->input('location')){
        $locationID = $r->input('location');
        $doctors = Doctor::where('location_id', $locationID)->select('id')->get();
        // return response()->json($doctors);
        return Visit::whereIn('doctor_id', $doctors)->with(['doctor', 'patient', 'procedure', 'product'])->get();
      }
      return Visit::with(['doctor', 'patient', 'procedure', 'product'])->get();
    }

    public function store(Request $r){
      $this->validate($r, [
        'start' => 'required',
        'end' => 'required',
        'patient_id' => 'required',
        'doctor_id' => 'required'
      ]);

      Visit::create($r->all());

      return response()->json('Vizitas sukurtas!');
    }

    public function cancel($id){
      $visit = Visit::find($id);
      $visit->filled = -1;
      $visit->save();

      return response()->json('Vizitas aštauktas!');
    }

    public function delete($id){
      Visit::find($id)->delete();

      return response()->json('Vizitas ištrintas!');
    }

    public function view($id){
      return Visit::where('id', $id)->with(['doctor', 'patient', 'patient.country', 'procedure.procedure', 'product.product'])->first();
    }

    public function procedureStore(Request $r, $id){
      $visit = Visit::find($id);
      $visit->filled = 1;
      $visit->save();

      $visit->procedure()->delete();

      foreach($r->all() as $item){
        $visit->procedure()->create(['visit_id' => $id, 'procedure_id' => $item['id']]);
      }

      return response()->json('Proceduros pridėtos');
    }

    public function productStore(Request $r, $id){
      $visit = Visit::find($id);
      $visit->filled = 1;
      $visit->save();

      $visit->product()->delete();

      foreach($r->all() as $item){
        $visit->product()->create(['visit_id' => $id, 'product_id' => $item['id']]);
      }

      return response()->json('Produktai pridėti');
    }
}
