<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Procedure;
use App\ProcedureCategory;

class ProcedureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function categoriesIndex(){
      return ProcedureCategory::with('procedures')->get();
    }

    public function categoryView($id){
      return ProcedureCategory::find($id);
    }

    public function storeCategory(Request $r){
      $this->validate($r, [
        'title' => 'required'
      ]);
      if(!$r->input('id')){
        ProcedureCategory::create($r->all());
      } else {
        ProcedureCategory::find($r->input('id'))->update($r->all());
      }

      return response()->json('Kategorija sukurta!');
    }

    public function deleteCategory($id){
      ProcedureCategory::find($id)->delete();

      return response()->json('Kategorija ištrinta!');
    }

    // Products
    public function procedureIndex(){
      return Procedure::with('category')->get();
    }

    public function procedureView($id){
      return Procedure::find($id);
    }

    public function procedureCategory(Request $r){
      $this->validate($r, [
        'title' => 'required',
        'price' => 'required'
      ]);
      if(!$r->input('id')){
        Procedure::create($r->all());
      } else {
        Procedure::find($r->input('id'))->update($r->all());
      }

      return response()->json('Procedūra pridėta!');
    }

    public function deleteProcedure($id){
      Procedure::find($id)->delete();

      return response()->json('Procedūra ištrinta!');
    }
}
