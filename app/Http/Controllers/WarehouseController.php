<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WarehouseCategory;
use App\Product;

class WarehouseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    //Categories
    public function categoriesIndex(){
      return WarehouseCategory::with('products')->get();
    }

    public function categoryView($id){
      return WarehouseCategory::find($id);
    }

    public function storeCategory(Request $r){
      $this->validate($r, [
        'title' => 'required'
      ]);
      if(!$r->input('id')){
        WarehouseCategory::create($r->all());
      } else {
        WarehouseCategory::find($r->input('id'))->update($r->all());
      }

      return response()->json('Kategorija sukurta!');
    }

    public function deleteCategory($id){
      WarehouseCategory::find($id)->delete();

      return response()->json('Kategorija ištrinta!');
    }

    // Products
    public function productIndex(){
      return Product::with('category')->get();
    }

    public function productView($id){
      return Product::find($id);
    }

    public function productCategory(Request $r){
      $this->validate($r, [
        'title' => 'required',
        'warehouse_category_id' => 'required',
        'qty' => 'integer',
        'unit' => 'required'
      ]);
      if(!$r->input('id')){
        Product::create($r->all());
      } else {
        Product::find($r->input('id'))->update($r->all());
      }

      return response()->json('Produktas pridėtas!');
    }

    public function deleteProduct($id){
      Product::find($id)->delete();

      return response()->json('Produktas ištrintas!');
    }
}
