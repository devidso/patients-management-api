<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(Request $r)
    {
      $this->validate($r, [
        'username' => 'required',
        'password' => 'required',
      ]);

      $user = User::where('username', $r->input('username'))->firstOrFail();

      if(Hash::check($r->input('password'), $user->password))
      {
        return response()->json('Correct!');
      } else {
        return response()->json('Incorrect!', 400);
      }
    }
}
