<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor;

class DoctorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $r){
      $doctors = Doctor::all();

      if($r->input('location')){
        $doctors = Doctor::where('location_id', $r->input('location'))->get();
      }

      return response()->json($doctors);
    }

    public function view($id){
      $doctor = Doctor::find($id);

      return response()->json($doctor);
    }

    public function store(Request $r){
      $this->validate($r, [
        'code' => 'required',
        'name' => 'required',
        'surname' => 'required',
        'birthday' => 'required',
        'address' => 'required',
        'email' => 'required|email',
        'country_id' => 'required',
      ], [
        'email.required' => 'El. pašto adresas yra būtinas'
      ]);

      Doctor::updateOrCreate(['code' => $r->input('code')], $r->all());

      return response()->json('Daktaras įtrauktas į sistemą!');
    }

    public function delete($id){
      Doctor::find($id)->delete();

      return response()->json('Daktaras ištrintas!');
    }
}
