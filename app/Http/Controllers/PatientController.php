<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patient;

class PatientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
      $patients = Patient::with(['country', 'visit'])->get();

      return response()->json($patients);
    }

    public function view($id){
      $patient = Patient::where('id', $id)->with(['country', 'visit'])->first();

      return response()->json($patient);
    }

    public function store(Request $r){
      $this->validate($r, [
        'code' => 'required',
        'name' => 'required',
        'surname' => 'required',
        'birthday' => 'required',
        'address' => 'required',
        'email' => 'required|email',
        'country_id' => 'required',
      ], [
        'email.required' => 'El. pašto adresas yra būtinas'
      ]);

      Patient::updateOrCreate(['code' => $r->input('code')], $r->all());

      return response()->json('Pacientas įtrauktas į sistemą!');
    }
}
