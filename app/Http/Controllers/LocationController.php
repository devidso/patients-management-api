<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;

class LocationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
      $locations = Location::all();

      return response()->json($locations);
    }

    public function view($id){
      $location = Location::find($id);

      return response()->json($location);
    }

    public function store(Request $r){
      $this->validate($r, [
        'name' => 'required|unique:locations',

      ]);
      Location::create($r->all());

      return response()->json('Naujas kabinetas įtrauktas į sistemą!');
    }

    public function patch(Request $r, $id){
      $this->validate($r, [
        'name' => 'required',

      ]);
      Location::find($id)->update($r->all());

      return response()->json('Naujas kabinetas įtrauktas į sistemą!');
    }

    public function delete($id){
      Location::find($id)->delete();

      return response()->json('Kabinetas ištrintas');
    }
}
