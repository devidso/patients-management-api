<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'surname', 'birthday', 'address', 'country_id', 'phone', 'gender', 'email'
    ];

    public function country(){
      return $this->belongsTo('App\Country');
    }

    public function visit(){
      return $this->hasMany('App\Visit');
    }
}
