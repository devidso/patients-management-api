<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function() use ($router) {
  //Auth
  $router->post('login', ['as' => 'login', 'uses' => 'AuthController@login']);

  //Patients
  $router->get('patient', ['as' => 'patientIndex', 'uses' => 'PatientController@index']);
  $router->get('patient/{id}', ['as' => 'patientIndex', 'uses' => 'PatientController@view']);
  $router->post('patient', ['as' => 'patientPost', 'uses' => 'PatientController@store']);
  $router->put('patient/{id}', ['as' => 'patientPut', 'uses' => 'PatientController@patch']);

  //Locations
  $router->get('location', ['as' => 'locationIndex', 'uses' => 'LocationController@index']);
  $router->get('location/{id}', ['as' => 'locationView', 'uses' => 'LocationController@view']);
  $router->post('location', ['as' => 'locationPost', 'uses' => 'LocationController@store']);
  $router->patch('location/{id}', ['as' => 'locationPost', 'uses' => 'LocationController@patch']);
  $router->delete('location/{id}', ['as' => 'locationDelete', 'uses' => 'LocationController@delete']);

  //Locations
  $router->get('doctor', ['as' => 'doctorIndex', 'uses' => 'DoctorController@index']);
  $router->get('doctor/{id}', ['as' => 'doctorView', 'uses' => 'DoctorController@view']);
  $router->post('doctor', ['as' => 'doctorPost', 'uses' => 'DoctorController@store']);
  $router->delete('doctor/{id}', ['as' => 'doctorDelete', 'uses' => 'DoctorController@delete']);

  //Warehouse
  $router->get('category', ['uses' => 'WarehouseController@categoriesIndex']);
  $router->get('category/{id}', ['uses' => 'WarehouseController@categoryView']);
  $router->post('category', ['uses' => 'WarehouseController@storeCategory']);
  $router->delete('category/{id}', ['uses' => 'WarehouseController@deleteCategory']);

  $router->get('product', ['uses' => 'WarehouseController@productIndex']);
  $router->get('product/{id}', ['uses' => 'WarehouseController@productView']);
  $router->post('product', ['uses' => 'WarehouseController@productCategory']);
  $router->delete('product/{id}', ['uses' => 'WarehouseController@deleteProduct']);

  //Procedures
  $router->get('procedure-category', ['uses' => 'ProcedureController@categoriesIndex']);
  $router->get('procedure-category/{id}', ['uses' => 'ProcedureController@categoryView']);
  $router->post('procedure-category', ['uses' => 'ProcedureController@storeCategory']);
  $router->delete('procedure-category/{id}', ['uses' => 'ProcedureController@deleteCategory']);

  $router->get('procedure', ['uses' => 'ProcedureController@procedureIndex']);
  $router->get('procedure/{id}', ['uses' => 'ProcedureController@procedureView']);
  $router->post('procedure', ['uses' => 'ProcedureController@procedureCategory']);
  $router->delete('procedure/{id}', ['uses' => 'ProcedureController@deleteProcedure']);

  //Visits
  $router->get('visit', ['uses' => 'VisitController@index']);
  $router->get('visit/{id}', ['uses' => 'VisitController@view']);
  $router->post('visit', ['uses' => 'VisitController@store']);
  $router->delete('visit/{id}', ['uses' => 'VisitController@delete']);
  $router->post('cancel-visit/{id}', ['uses' => 'VisitController@cancel']);

  //Visit Procedures
  $router->get('visit-procedure', ['uses' => 'VisitController@procedureIndex']);
  $router->get('visit-procedure/{id}', ['uses' => 'VisitController@procedureView']);
  $router->post('visit-procedure/{id}', ['uses' => 'VisitController@procedureStore']);
  $router->delete('visit-procedure/{id}', ['uses' => 'VisitController@procedureDelete']);

  //Visti Products
  $router->get('visit-product', ['uses' => 'VisitController@productIndex']);
  $router->get('visit-product/{id}', ['uses' => 'VisitController@productView']);
  $router->post('visit-product/{id}', ['uses' => 'VisitController@productStore']);
  $router->delete('visit-product/{id}', ['uses' => 'VisitController@productDelete']);
});
